//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2015 Sandia Corporation.
//  Copyright 2015 UT-Battelle, LLC.
//  Copyright 2015 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_internal_ArrayPortalUniformPointCoordinates2D_h
#define vtk_m_internal_ArrayPortalUniformPointCoordinates2D_h

#include <vtkm/Assert.h>
#include <vtkm/Types.h>

namespace vtkm {
namespace internal {

/// \brief An implicit array port that computes point coordinates for a uniform grid in 2D.
///
class VTKM_ALWAYS_EXPORT ArrayPortalUniformPointCoordinates2D
{
public:
  typedef vtkm::Vec<vtkm::FloatDefault,2> ValueType;

  VTKM_EXEC_CONT
  ArrayPortalUniformPointCoordinates2D() : NumberOfValues(0) {  }

  VTKM_EXEC_CONT
  ArrayPortalUniformPointCoordinates2D(vtkm::Id2 dimensions,
                                     ValueType origin,
                                     ValueType spacing)
    : Dimensions(dimensions),
      NumberOfValues(dimensions[0]*dimensions[1]),
      Origin(origin),
      Spacing(spacing)
  {  }

  VTKM_EXEC_CONT
  ArrayPortalUniformPointCoordinates2D(
      const ArrayPortalUniformPointCoordinates2D &src)
    : Dimensions(src.Dimensions),
      NumberOfValues(src.NumberOfValues),
      Origin(src.Origin),
      Spacing(src.Spacing)
  {  }

  VTKM_EXEC_CONT
  ArrayPortalUniformPointCoordinates2D &
  operator=(const ArrayPortalUniformPointCoordinates2D &src)
  {
    this->Dimensions = src.Dimensions;
    this->NumberOfValues = src.NumberOfValues;
    this->Origin = src.Origin;
    this->Spacing = src.Spacing;
    return *this;
  }

  VTKM_EXEC_CONT
  vtkm::Id GetNumberOfValues() const { return this->NumberOfValues; }

  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id index) const {
    VTKM_ASSERT(index >= 0);
    VTKM_ASSERT(index < this->GetNumberOfValues());
    return this->Get(
          vtkm::Id2(index%this->Dimensions[0],
                    index/this->Dimensions[0]));
  }

  VTKM_EXEC_CONT
  vtkm::Id2 GetRange2() const { return this->Dimensions; }

  VTKM_EXEC_CONT
  ValueType Get(vtkm::Id2 index) const {
    VTKM_ASSERT((index[0] >= 0) && (index[1] >= 0));
    VTKM_ASSERT((index[0] < this->Dimensions[0]) &&
                (index[1] < this->Dimensions[1]));
    return ValueType(this->Origin[0] + this->Spacing[0] * static_cast<vtkm::FloatDefault>(index[0]),
                     this->Origin[1] + this->Spacing[1] * static_cast<vtkm::FloatDefault>(index[1]));
  }

  VTKM_EXEC_CONT
  const vtkm::Id2 &GetDimensions() const { return this->Dimensions; }

  VTKM_EXEC_CONT
  const ValueType &GetOrigin() const { return this->Origin; }

  VTKM_EXEC_CONT
  const ValueType &GetSpacing() const { return this->Spacing; }

private:
  vtkm::Id2 Dimensions;
  vtkm::Id NumberOfValues;
  ValueType Origin;
  ValueType Spacing;
};

}
} // namespace vtkm::internal

#endif //vtk_m_internal_ArrayPortalUniformPointCoordinates2D_h
