//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================

#include <vtkm/cont/ArrayHandleUniformPointCoordinates2D.h>

#include <vtkm/cont/testing/Testing.h>

namespace {

typedef vtkm::Vec<vtkm::FloatDefault, 2> Vector2;

const vtkm::Id2 DIMENSIONS(16, 18);
const vtkm::Id NUM_POINTS = 288;

const Vector2 ORIGIN(-20, 5);
const Vector2 SPACING(10, 1);

void TestArrayHandleUniformPointCoordinates2D()
{
  std::cout << "Creating ArrayHandleUniformPointCoordinates2D" << std::endl;

  vtkm::cont::ArrayHandleUniformPointCoordinates2D arrayHandle(
        DIMENSIONS, ORIGIN, SPACING);
  VTKM_TEST_ASSERT(arrayHandle.GetNumberOfValues() == NUM_POINTS,
                   "Array computed wrong number of points.");

  std::cout << "Getting array portal." << std::endl;
  vtkm::internal::ArrayPortalUniformPointCoordinates2D portal =
      arrayHandle.GetPortalConstControl();
  VTKM_TEST_ASSERT(portal.GetNumberOfValues() == NUM_POINTS,
                   "Portal has wrong number of points.");
  VTKM_TEST_ASSERT(portal.GetRange2() == DIMENSIONS,
                   "Portal range is wrong.");

  std::cout << "Checking computed values of portal." << std::endl;
  Vector2 expectedValue;
  vtkm::Id flatIndex = 0;
  vtkm::Id2 blockIndex;
  expectedValue[1] = ORIGIN[1];
  for (blockIndex[1] = 0; blockIndex[1] < DIMENSIONS[1]; blockIndex[1]++)
  {
    expectedValue[0] = ORIGIN[0];
    for (blockIndex[0] = 0; blockIndex[0] < DIMENSIONS[0]; blockIndex[0]++)
    {
      VTKM_TEST_ASSERT(test_equal(expectedValue, portal.Get(flatIndex)),
                       "Got wrong value for flat index.");

      VTKM_TEST_ASSERT(test_equal(expectedValue, portal.Get(blockIndex)),
                       "Got wrong value for block index.");

      flatIndex++;
      expectedValue[0] += SPACING[0];
    }
    expectedValue[1] += SPACING[1];
  }
}

} // anonymous namespace

int UnitTestArrayHandleUniformPointCoordinates2D(int, char *[])
{
  return vtkm::cont::testing::Testing::Run(
        TestArrayHandleUniformPointCoordinates2D);
}
